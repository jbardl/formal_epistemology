# Pairwise coordination game

Juego en que cada agente de una población intenta coordinar con alguno de sus vecinos. El fenómeno emergente de la simulación es que, dependiendo de la estructura de la red subyacente, los agentes alcanzan un consenso sobre qué estrategia utilizar.