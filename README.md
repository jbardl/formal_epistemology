# Epistemología formal

Este repositorio incluye dos proyectos de epistemología social:
- [Juego de coordinación de pares](https://gitlab.com/jbardl/formal_epistemology/-/tree/master/social_epistemology/pairwise_coordination): juego que modela la difusión de convenciones en redes de agentes.
- [Segregación de Schelling](https://gitlab.com/jbardl/formal_epistemology/-/tree/master/social_epistemology/schelling_seggregation): modelo clásico de patrones de segregación en poblaciones cuyos habitantes no son especialmente discriminadores o segregadores.
